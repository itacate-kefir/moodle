Mientras el curso está abierto, las participantes pueden ver sus calificaciones yendo a su Perfil.  En el menú "Mis cursos", se despliega el Perfil de participantes, con un listado de cursos activos.  En la barra principal, una de las opciones es "Calificaciones".  En ese apartado pueden verlas individualmente y el total.

# Finalizar el curso

En el menú de administración (el logo del engranaje), dentro de "Administración de curso" hay una opción "Finalización del curso".  En este apartado se pueden establecer las condiciones de finalización, por ejemplo alcanzar una nota, completar las actividades, etc.

Si no se configuraron las condiciones antes de que algunas participantes lleguen a terminar el curso, Moodle deshabilita las opciones, pero muestra un botón que permite desbloquearlas.

A continuación se muestra un listado de condiciones.  Las opciones de cada una pueden desplegarse (o replegarse) haciendo click en el título de cada una (es un "acordeón").

## Requisitos de finalización

Esta opción permite seleccionar si queremos que se cumpla alguna de las condiciones o todas para poder terminar el curso.

## Actividades finalizadas

Muestra un listado de todas las actividades del cursos, las que estén marcadas son obligatorias para poder terminar el curso.

## Dependencias finalizadas

Solo se puede finalizar este curso si se han completado otros cursos

## Fecha

La matriculación termina en la fecha especificada.

## Período de tiempo de la matrícula

En lugar de una fecha, se puede establecer una cantidad de días corridos en los que se puede participar del curso.

## Dar de baja

El curso se marca como finalizado cuando la Participante se da de baja.

## Calificación del curso

El curso se finaliza cuando se alcanza una calificación determinada.

## Autocompletar manualmente

La participante puede finalizar el curso manualmente en el momento en que desee.

## Finalización manual por otrxs

Otros roles que pueden marcar el curso como finalizado (Docentes, Tutoras, etc.)